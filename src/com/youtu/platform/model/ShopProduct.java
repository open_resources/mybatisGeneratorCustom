package com.youtu.platform.model;

import java.math.BigDecimal;
import java.util.Date;

/**
 * 商品信息表
 * 
 * @author wcyong
 * 
 * @date 2019-02-26
 */
public class ShopProduct {
    /**
     * 商品ID
     */
    private Integer productId;

    /**
     * 商品编码
     */
    private String productCode;

    /**
     * 商品名称
     */
    private String productName;

    /**
     * 数量
     */
    private Integer numberof;

    /**
     * 商品售价价格kb
     */
    private BigDecimal price;

    /**
     * 上下架状态:0下架，1上架
     */
    private Byte publishStatus;

    /**
     * 是否广告图:0否，1是
     */
    private Byte advertising;

    /**
     * 生产日期
     */
    private Date productionDate;

    /**
     * 商品有效期
     */
    private Integer shelfLife;

    /**
     * 商品描述1
     */
    private String descript1;

    /**
     * 商品描述
     */
    private String descript;

    /**
     * 商品描述2
     */
    private String descript2;

    /**
     * 长图片URL
     */
    private String longPicUrl;

    /**
     * 方图片URL
     */
    private String squarePicUrl;

    /**
     * 最后修改时间
     */
    private Date modifiedTime;

    /**
     * 详情图url
     */
    private String detailsPicUrl;

    public Integer getProductId() {
        return productId;
    }

    public void setProductId(Integer productId) {
        this.productId = productId;
    }

    public String getProductCode() {
        return productCode;
    }

    public void setProductCode(String productCode) {
        this.productCode = productCode == null ? null : productCode.trim();
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName == null ? null : productName.trim();
    }

    public Integer getNumberof() {
        return numberof;
    }

    public void setNumberof(Integer numberof) {
        this.numberof = numberof;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public Byte getPublishStatus() {
        return publishStatus;
    }

    public void setPublishStatus(Byte publishStatus) {
        this.publishStatus = publishStatus;
    }

    public Byte getAdvertising() {
        return advertising;
    }

    public void setAdvertising(Byte advertising) {
        this.advertising = advertising;
    }

    public Date getProductionDate() {
        return productionDate;
    }

    public void setProductionDate(Date productionDate) {
        this.productionDate = productionDate;
    }

    public Integer getShelfLife() {
        return shelfLife;
    }

    public void setShelfLife(Integer shelfLife) {
        this.shelfLife = shelfLife;
    }

    public String getDescript1() {
        return descript1;
    }

    public void setDescript1(String descript1) {
        this.descript1 = descript1 == null ? null : descript1.trim();
    }

    public String getDescript() {
        return descript;
    }

    public void setDescript(String descript) {
        this.descript = descript == null ? null : descript.trim();
    }

    public String getDescript2() {
        return descript2;
    }

    public void setDescript2(String descript2) {
        this.descript2 = descript2 == null ? null : descript2.trim();
    }

    public String getLongPicUrl() {
        return longPicUrl;
    }

    public void setLongPicUrl(String longPicUrl) {
        this.longPicUrl = longPicUrl == null ? null : longPicUrl.trim();
    }

    public String getSquarePicUrl() {
        return squarePicUrl;
    }

    public void setSquarePicUrl(String squarePicUrl) {
        this.squarePicUrl = squarePicUrl == null ? null : squarePicUrl.trim();
    }

    public Date getModifiedTime() {
        return modifiedTime;
    }

    public void setModifiedTime(Date modifiedTime) {
        this.modifiedTime = modifiedTime;
    }

    public String getDetailsPicUrl() {
        return detailsPicUrl;
    }

    public void setDetailsPicUrl(String detailsPicUrl) {
        this.detailsPicUrl = detailsPicUrl == null ? null : detailsPicUrl.trim();
    }
}