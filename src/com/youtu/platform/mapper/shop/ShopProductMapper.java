package com.youtu.platform.mapper.shop;

import com.youtu.platform.model.ShopProduct;

public interface ShopProductMapper {
    int deleteByPrimaryKey(Integer productId);

    int insert(ShopProduct record);

    int insertSelective(ShopProduct record);

    ShopProduct selectByPrimaryKey(Integer productId);

    int updateByPrimaryKeySelective(ShopProduct record);

    int updateByPrimaryKey(ShopProduct record);
}